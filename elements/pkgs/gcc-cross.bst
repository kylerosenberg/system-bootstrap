kind: autotools

# TODO: Remove refs from each source

sources:
- kind: gnu
  name: gcc
  url: https://ftpmirror.gnu.org/gnu/gcc/gcc-12.1.0/gcc-12.1.0.tar.xz
  sha256sum: 62fd634889f31c02b64af2c468f064b47ad1ca78411c45abe6ac4b5f8dd19c7b
- kind: gnu
  name: gmp
  directory: gmp
  url: https://ftpmirror.gnu.org/gnu/gmp/gmp-6.2.1.tar.xz
  sha256sum: fd4829912cddd12f84181c3451cc752be224643e87fac497b69edddadc49b4f2
- kind: gnu
  name: mpfr
  directory: mpfr
  url: https://ftpmirror.gnu.org/gnu/mpfr/mpfr-4.1.0.tar.xz
  sha256sum: 0c98a3f1732ff6ca4ea690552079da9c597872d30e96ec28414ee23c95558a7f
- kind: gnu
  name: mpc
  directory: mpc
  url: https://ftpmirror.gnu.org/gnu/mpc/mpc-1.2.1.tar.gz
  sha256sum: 17503d2c395dfcf106b622dc142683c1199431d095367c6aacba6eec30340459

build-depends:
- tools/import-fdo.bst
- pkgs/binutils-cross.bst
- tools/sysroot-setup.bst

variables:
  build-dir: build
  conf-local: >-
    --target=%{bootstrap-triplet}
    --with-sysroot=%{bootstrap-sysroot}
    --with-glibc-version=2.11
    --with-newlib
    --without-headers
    --with-local-prefix=%{prefix}
    --with-native-system-header-dir=%{includedir}
    --disable-nls
    --disable-shared
    --disable-multilib
    --disable-decimal-float
    --disable-threads
    --disable-libatomic
    --disable-libgomp
    --disable-libmpx
    --disable-libquadmath
    --disable-libssp
    --disable-libvtv
    --disable-libstdcxx
    --enable-deterministic-archives
    --enable-linker-build-id
    --enable-languages=c,c++

config:
  # See: http://www.linuxfromscratch.org/lfs/view/stable-systemd/chapter05/gcc-pass1.html
  # I changed the first sed command to always point the dynamic linker to %{libdir}/ld-linux-...
  configure-commands:
    (<):
    - |
      for file in gcc/config/{linux,i386/linux{,64}}.h; do
        cp -v $file{,.orig}
        sed -e 's@/lib\(64\)\?\(32\)\?/ld@%{libdir}/ld@g' \
            -e 's@/usr@%{prefix}@g' $file.orig > $file
        echo '#undef STANDARD_STARTFILE_PREFIX_1' >> $file
        echo '#undef STANDARD_STARTFILE_PREFIX_2' >> $file
        echo '#define STANDARD_STARTFILE_PREFIX_1 "%{libdir}/"' >> $file
        echo '#define STANDARD_STARTFILE_PREFIX_2 ""' >> $file
      done
    - sed -e '/m64=/s/lib64/lib/' -i gcc/config/i386/t-linux64
