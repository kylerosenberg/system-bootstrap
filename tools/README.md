# Tools

Contains various helper scripts that make building and installing carbonOS easier.

Scripts:
- `tools/checkout`: Checkout out artifacts out of Buildstream and into result/
- `tools/update`: Updates all the packages in the OS
- `tools/manual-update`: Updates packages that require manual editing of the build file. Part of `tools/update`
